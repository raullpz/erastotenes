﻿using System;
using System.Collections.Generic;
using System.Text;

namespace erastotenes
{
    public interface ILista
    {

        void CrearListaDe2AN(int n);

        List<NumeroMarcado> GetLista();

        int SiguienteNumeroNoMarcado();

        void MarcarMultiplos(int numero);

    }
}
