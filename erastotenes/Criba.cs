﻿using System;
using System.Collections.Generic;
using System.Text;



namespace erastotenes
{
    public class Criba
    {

        private ILista ListNumber;

        public Criba()
        {
            ListNumber = new Lista();
        }


        public List<NumeroMarcado> GetLista()
        {
            return (ListNumber.GetLista());
        }

        public void CrearListaDe2AN(int n)
        {

            ListNumber.CrearListaDe2AN(n);

        }

        public int SiguienteNumeroNoMarcado()
        {

            return(ListNumber.SiguienteNumeroNoMarcado());

        }

        public void MarcarMultiplos (int numero)
        {
            ListNumber.MarcarMultiplos(numero);
        }






    }
}
