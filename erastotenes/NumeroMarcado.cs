﻿using System;
using System.Collections.Generic;
using System.Text;

namespace erastotenes
{
    public class NumeroMarcado
    {

        private int numero;
        private int estado;


        public NumeroMarcado(int numero, int estado)
        {
            this.numero = numero;
            this.estado = estado;
        }

        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        public int Estado
        {
            get { return estado; }
            set { estado = value; }
        }

    }
}
