﻿using System;
using System.Collections.Generic;
using System.Text;

namespace erastotenes
{
    public class Lista : ILista
    {

        private List<NumeroMarcado> ListNumber;

        public void CrearListaDe2AN(int n)
        {

            ListNumber = new List<NumeroMarcado>();

            for (int i = 2; i <= n; i++)
                ListNumber.Add(new NumeroMarcado(i, 2));

        }

        public List<NumeroMarcado> GetLista()
        {
            return (ListNumber);
        }

        public int SiguienteNumeroNoMarcado()
        {
            List<NumeroMarcado> ListaNoMarcados = new List<NumeroMarcado>();

            foreach (NumeroMarcado item in ListNumber)
            {

                if (item.Estado == 2)
                {
                    item.Estado = 1;
                    return (item.Numero);
                }
            }

            return (-1);

        }

        public void MarcarMultiplos(int numero)
        {

            foreach (NumeroMarcado item in ListNumber)
            {
                if (item.Numero == numero)
                    item.Estado = 1;
                else if ((item.Estado == 2) && ((item.Numero % numero).Equals(0)))
                    item.Estado = 0;

            }

        }

    }
}
