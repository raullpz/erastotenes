using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using erastotenes;

namespace erastotenesTest
{
    [TestClass]
    public class CribaTest
    {
        private Criba ClCriba;


        [TestMethod]
        public void CrearListaDe2AN()
        {
            //Arrange
            ClCriba = new Criba();


            //Act 
            ClCriba.CrearListaDe2AN(5);

            //Assert 
            Assert.AreEqual(ClCriba.GetLista()[0].Numero, 2);
            Assert.AreEqual(ClCriba.GetLista()[ClCriba.GetLista().Count - 1].Numero, 5);

        }

        [TestMethod]
        public void SiguienteNumeroNoMarcado()
        {
            int numero;

            //Arrange
            ClCriba = new Criba();

            //Act 
            ClCriba.CrearListaDe2AN(5);

            numero = ClCriba.SiguienteNumeroNoMarcado();

            //Assert 
            Assert.AreEqual(numero,2);
        }

        [TestMethod]
        public void MarcarMultiplos()
        {
            
            //Arrange
            ClCriba = new Criba();

            //Act 
            ClCriba.CrearListaDe2AN(5);            
            ClCriba.MarcarMultiplos(ClCriba.SiguienteNumeroNoMarcado());

            //Assert 
            Assert.AreEqual(ClCriba.GetLista().Find(x => x.Numero.Equals(4)).Estado, 0);
            
        }

        [TestMethod]
        public void crearListaPrimos()
        {
            int numero=2;

            //Arrange
            ClCriba = new Criba();

            //Act 
            ClCriba.CrearListaDe2AN(5);

            while (numero > -1)
            {

                ClCriba.MarcarMultiplos(numero);

                numero = ClCriba.SiguienteNumeroNoMarcado();                

            }

            //Assert 
            Assert.AreEqual(ClCriba.GetLista().Find(x => x.Numero.Equals(2)).Estado, 1);
            Assert.AreEqual(ClCriba.GetLista().Find(x => x.Numero.Equals(3)).Estado, 1);
            Assert.AreEqual(ClCriba.GetLista().Find(x => x.Numero.Equals(4)).Estado, 0);
            Assert.AreEqual(ClCriba.GetLista().Find(x => x.Numero.Equals(5)).Estado, 1);

        }

    }
}
